package txtsync

import (
	"errors"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"sync"
)

// EditionsModel model
type EditionsModel struct {
	AppendEnabled  bool
	ReplaceEnabled bool
	Replace        struct {
		Old string
		New string
	}
	Append struct {
		Text string
	}
}

var editions = make(chan EditionsModel)
var startIs = false
var zonasTxt *os.File
var folderFile string

// TextZonasTxt text
var TextZonasTxt string
var complete sync.WaitGroup

// UpdateGet text
func UpdateGet() error {
	fil, err := ioutil.ReadFile(folderFile)
	if err != nil {
		return err
	}
	TextZonasTxt = string(fil)
	return err
}

// Start App
func Start(folder string) (err error) {
	folderFile = folder
	//  Start is enabled
	if startIs == true {
		log.Println("txtsync: This Start Before")
		return nil
	}
	startIs = true
	// Get Zonas Txt
	zonasTxt, err = os.OpenFile(folder, os.O_RDWR, 0644)
	if err != nil {
		startIs = false
		return err
	}
	// Get All
	fil, err := ioutil.ReadAll(zonasTxt)
	if err != nil {
		startIs = false
		return err
	}
	TextZonasTxt = string(fil)
	go func() {
		for {
			ed := <-editions
			// Is Append Text
			if ed.AppendEnabled {
				TextZonasTxt += ed.Append.Text
				zonasTxt.WriteAt([]byte(TextZonasTxt), 0)
			}
			// Is Replace Text
			if ed.ReplaceEnabled {
				TextZonasTxt = strings.Replace(TextZonasTxt, ed.Replace.Old, ed.Replace.New, 1)
				zonasTxt.WriteAt([]byte(TextZonasTxt), 0)
			}
			complete.Done()
		}
	}()
	return nil
}

func Replace(OldText string, NewText string) error {
	if startIs == false {
		return errors.New("txtsync: Not Start")
	}
	var ed EditionsModel
	ed.ReplaceEnabled = true
	ed.Replace.Old = OldText
	ed.Replace.New = NewText
	complete.Add(1)
	editions <- ed
	complete.Wait()
	return nil
}
func Append(NewText string) error {
	if startIs == false {
		return errors.New("txtsync: Not Start")
	}
	var ed EditionsModel
	ed.AppendEnabled = true
	ed.Append.Text = NewText
	complete.Add(1)
	editions <- ed
	complete.Wait()
	return nil
}
