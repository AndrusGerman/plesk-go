package zonastxtedit

import (
	"errors"
	"strings"

	"gitlab.com/AndrusGerman/plesk-go/pkg/txtsync"
)

// Start App
func Start(folder string) error {
	return txtsync.Start(folder)
}

// AddDomainZonasTxt to zonas txt
func AddDomainZonasTxt(domain string, ip string) error {
	cont := domainZonasTxt(domain, ip)
	err := txtsync.UpdateGet()
	if err != nil {
		return err
	}
	if strings.Contains(txtsync.TextZonasTxt, domain+`.|master`) {
		return errors.New("This domain is exist in ZonasTxt")
	}
	return txtsync.Append("\n" + cont)
}

// AddSubDomainZonasTxt to zonas txt
func AddSubDomainZonasTxt(domain string, subDomain string, ip string) error {
	err := txtsync.UpdateGet()
	if err != nil {
		return err
	}
	old, newt, err := subDomainZonasTxt(txtsync.TextZonasTxt, domain, subDomain, ip)
	if err != nil {
		return err
	}
	return txtsync.Replace(
		old, newt,
	)
}

func domainZonasTxt(domain string, ip string) string {
	return domain + `.|master {
|CNAME| |www.` + domain + `.| |` + domain + `.| ||
|TXT| |_dmarc.` + domain + `.| |v=DMARC1; p=none| ||
|TXT| |` + domain + `.| |v=spf1 +a +mx +a:localhost.localdomain -all| ||
|CNAME| |ftp.` + domain + `.| |` + domain + `.| ||
|A| |ipv4.` + domain + `.| |` + ip + `| ||
|A| |mail.` + domain + `.| |` + ip + `| ||
|MX| |` + domain + `.| |mail.` + domain + `.| |10|
|A| |webmail.` + domain + `.| |` + ip + `| ||
|A| |` + domain + `.| |` + ip + `| ||
|A| |ns2.` + domain + `.| |` + ip + `| ||
|NS| |` + domain + `.| |ns6.nsprimario.com.| ||
|A| |ns1.` + domain + `.| |` + ip + `| ||
|NS| |` + domain + `.| |ns5.nsprimario.com.| ||
}
`
}

func subDomainZonasTxt(zTxt string, domain string, subDomain string, ip string) (OldTxt string, newText string, err error) {
	var cont string
	_, cont, err = getLine(zTxt, "|A| |"+domain+".| |")
	if err != nil {
		return "", "", err
	}
	TheNew := "|A| |" + subDomain + "." + domain + ".| |" + ip + "| ||"

	if strings.Contains(zTxt, TheNew) {
		err = errors.New("This subdomain is exist")
		return
	}
	return cont, cont + "\n" + TheNew, nil
}

func getLine(txt string, cont string) (int, string, error) {
	arr := strings.Split(txt, "\n")
	for ind, val := range arr {
		if strings.Contains(val, cont) {
			return ind, val, nil
		}
	}
	return -1, "", errors.New("Linea no encontrada '" + cont + "' in '" + txt + "'")
}
