package plesk

import (
	"errors"
	"os/exec"
	"strings"
)

//func example() {
//	var deli, err = NewPleskSubscritionCLI("deliveryplan.es", "46.231.3.42")
//	if err != nil {
//		return
//	}
//	defer deli.Close()
//	deli.CreateSubDomainZonasTXT("deliveryplan.es", "mipizza")
//	deli.DeleteSubDomainZonasTXT("deliveryplan.es", "mipizza")
//}

func getLine(txt string, cont string) (int, string, error) {
	arr := strings.Split(txt, "\n")
	for ind, val := range arr {
		if strings.Contains(val, cont) {
			return ind, val, nil
		}
	}
	return -1, "", errors.New("Linea no encontrada")
}

func osComand(comand string) ([]byte, error) {
	return exec.Command("sh", "-c", comand).Output()
}

func byteToString(a []byte, b error) (string, error) {
	return string(a), b
}
