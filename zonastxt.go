package plesk

import (
	"errors"
	"log"
	"strings"

	"gitlab.com/AndrusGerman/plesk-go/pkg/zonastxtedit"
)

// ErrDomainIsNull | Domain is Null
var ErrDomainIsNull = errors.New("Domain is Null")

// ErrSubDomainIsNull | SubDomain is Null
var ErrSubDomainIsNull = errors.New("SubDomain is Null")

// ErrSubDomainIsNotValid | SubDomain is Not Valid
var ErrSubDomainIsNotValid = errors.New("SubDomain is Not Valid")

// Close ZonsTXT files
func (ctx *ZonasTXT) Close() {

}

// ReadZonasTXT files
func ReadZonasTXT(folder string) (*ZonasTXT, error) {
	z := &ZonasTXT{}
	return z, zonastxtedit.Start(folder)
}

// ZonasTXT is a class for zonas.txt files
type ZonasTXT struct {
}

func (ctx *ZonasTXT) createSubDomain(domain string, subDomain string, ip string) error {
	if subDomain == "" {
		return ErrSubDomainIsNull
	}
	if domain == "" {
		return ErrDomainIsNull
	}
	if strings.Contains(".", subDomain) {
		return ErrSubDomainIsNotValid
	}
	return zonastxtedit.AddSubDomainZonasTxt(domain, subDomain, ip)
}

func (ctx *ZonasTXT) createDomain(domain string, ip string) error {
	if domain == "" {
		return ErrDomainIsNull
	}
	return zonastxtedit.AddDomainZonasTxt(domain, ip)
}

func (ctx *ZonasTXT) deleteSubDomain(domain string, subDomain string) error {
	log.Println("deleteSubDomain: Not Complete")
	return nil
}
