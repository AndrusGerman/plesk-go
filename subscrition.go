package plesk

import "errors"

// Subscrition model
type Subscrition struct {
	Subscrition string
	IP          string
	zonasTXT    *ZonasTXT
}

// NewSubscritionCLI create new plesk subscrition CLI
func NewSubscritionCLI(subscrition string, ip string, zonasTXT string) (*Subscrition, error) {
	var z, err = ReadZonasTXT(zonasTXT)
	return &Subscrition{
		Subscrition: subscrition,
		IP:          ip,
		zonasTXT:    z,
	}, err
}

// Close Plesk class
func (ctx *Subscrition) Close() {
	ctx.zonasTXT.Close()
}

// CreateSubDomainZonasTXT in zonas txt
func (ctx *Subscrition) CreateSubDomainZonasTXT(domain string, subDomain string) error {
	if ctx.zonasTXT == nil {
		return errors.New("ZonasTXT no esta iniciado")
	}
	return ctx.zonasTXT.createSubDomain(domain, subDomain, ctx.IP)
}

// CreateDomainZonasTXT in zonas txt
func (ctx *Subscrition) CreateDomainZonasTXT(domain string) error {
	if ctx.zonasTXT == nil {
		return errors.New("ZonasTXT no esta iniciado")
	}
	return ctx.zonasTXT.createDomain(domain, ctx.IP)
}

// DeleteSubDomainZonasTXT in zonas txt
func (ctx *Subscrition) DeleteSubDomainZonasTXT(domain string, subDomain string) error {
	if ctx.zonasTXT == nil {
		return errors.New("ZonasTXT no esta iniciado")
	}
	return ctx.zonasTXT.deleteSubDomain(domain, subDomain)
}

// Domain cli
func (ctx *Subscrition) Domain(domain string) (*Domain, error) {
	return NewCliDomain(domain, ctx.Subscrition)
}

// EditionsModel is model for editions
type EditionsModel struct {
	OldCont string
	NewCont string
	Append  bool
}
