package plesk

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

// Domain model
type Domain struct {
	Domain              string
	WebSpaceName        string
	SubDomain           bool
	ForceNotCertificate bool
}

// Create domain
func (ctx *Domain) Create() (string, error) {
	if ctx.SubDomain {
		return ctx.CreateSubDomain()
	}
	return ctx.CreateDomain()
}

// CreateZonasTXT ZONASTXT
func (ctx *Domain) CreateZonasTXT(sub *Subscrition, domain string, subDomain string) error {
	if ctx.SubDomain {
		return sub.CreateSubDomainZonasTXT(domain, subDomain)
	}
	return sub.CreateDomainZonasTXT(domain)
}

// CreateDomain in plesk cli
func (ctx *Domain) CreateDomain() (string, error) {
	b, err := osComand(fmt.Sprintf("plesk bin site --create %s -webspace-name %s -www-root %s -ssl true -hosting true", ctx.Domain, ctx.WebSpaceName, ctx.Domain))
	return string(b), err
}

// CreateSubDomain in plesk
func (ctx *Domain) CreateSubDomain() (string, error) {
	var comand = "plesk bin subdomain -c " + ctx.Domain + " -www-root /" + ctx.Domain + " -domain " + ctx.WebSpaceName
	return byteToString(osComand(comand))
}

// CreateLetsencrypt in plesk cli custom comand
// [FULL] is full domain
// DOM is domain
// SUBS is WebSpaceName
func (ctx *Domain) CreateLetsencrypt(comand string) ([]byte, error) {
	if ctx.ForceNotCertificate {
		return []byte(`ForceNotCertificate is enabled`), nil
	}
	f := ctx.Domain
	if ctx.IsSubDomain() {
		f = ctx.Domain + "." + ctx.WebSpaceName
	}
	rp := strings.NewReplacer(
		"DOM", ctx.Domain,
		"[FULL]", f,
		"SUBS", ctx.WebSpaceName,
	)

	return osComand(rp.Replace(comand))
}

// CreateLetsencryptDefault comand
func (ctx *Domain) CreateLetsencryptDefault() ([]byte, error) {
	return ctx.CreateLetsencrypt(`plesk bin extension --exec letsencrypt cli.php -d [FULL] -m jm@hementxe.com`)
}

// NewCliDomain manager
func NewCliDomain(domain string, subscrition string) (*Domain, error) {
	domain = strings.Replace(domain, " ", "", 2)
	if domain == "" {
		return nil, errors.New("Err domain is null")
	}
	return &Domain{Domain: domain, WebSpaceName: "deliveryplan.es"}, nil
}

// SetHTML in one domain
func (ctx *Domain) SetHTML(html string) error {
	d1 := []byte(html)
	os.MkdirAll("/var/www/vhosts/"+ctx.WebSpaceName+"/"+ctx.Domain+"/", 0644)
	return ioutil.WriteFile("/var/www/vhosts/"+ctx.WebSpaceName+"/"+ctx.Domain+"/index.html", d1, 0644)
}

// SetHTTPSHTACCESS by htaccess
func (ctx *Domain) SetHTTPSHTACCESS() error {
	d1 := []byte(`
	RewriteEngine On
	RewriteCond %{HTTPS} off
	RewriteRule ^(.*)$ https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]
	# RewriteCond %{HTTPS} off [OR]
	# RewriteCond %{HTTP_HOST} !^www\. [OR]
	# RewriteCond %{HTTP_HOST} ^` + ctx.Domain + `\.com$ [NC]
	# RewriteRule ^(.*)$ https://` + ctx.Domain + `/$1 [R=301,L]`)
	return ioutil.WriteFile("/var/www/vhosts/"+ctx.WebSpaceName+"/"+ctx.Domain+"/.htaccess", d1, 0644)
}

// IsSubDomain is sub domain
func (ctx *Domain) IsSubDomain() bool {
	return !strings.Contains(ctx.Domain, ".")
}
